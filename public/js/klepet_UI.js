/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  slikeHtml = izlusciSlike(sporocilo);
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      if(slikeHtml){
        $('#sporocila').append(slikeHtml);
      }
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    if(slikeHtml){
      $('#sporocila').append(slikeHtml);
    }
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}

function izlusciSlike(sporocilo) {
  var re = new RegExp('(http)[^ ]*\.(jpe?g|png|gif)', 'gi');
  var povezaveDoSlik = sporocilo.match(re);
  if(!povezaveDoSlik){
    return false;
  }
  
  slikeHtml = "<br>";
  for(var i in povezaveDoSlik){
    slikeHtml += "<img src='" + povezaveDoSlik[i] + "' style='width:200px; margin-left:20px;' /><br>";
  }
  
  return slikeHtml;
}

var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var jeSimbol = false;
    var jeZasebno = false;
    var novElement;
    var vzdevek;
    var uporabnik;
    var novoSporocilo;
    var res = sporocilo.besedilo.split(" ");
    
    for(var i in res){
      if(res[i].substring(0, 2) == "&#"){
        jeSimbol = true;
        break;
      }
    }
    
    if(res[1] == "(zasebno):"){
        uporabnik = res[0];
        jeZasebno = true;
        vzdevek = aliImaLokalniVzdevek(uporabnik);
        if(!vzdevek){
          if(jeSimbol){
            novElement = document.createElement('div');
            novElement.innerHTML = sporocilo.besedilo;
          }
          else{
            novElement = divElementEnostavniTekst(sporocilo.besedilo);
          }
        }
        else{
          res[0] = vzdevek + ' (' + uporabnik + ')';
          novoSporocilo = res.join(" ");
          if(jeSimbol){
            novElement = document.createElement('div');
            novElement.innerHTML = novoSporocilo;
          }
          else{
            novElement = divElementEnostavniTekst(novoSporocilo);
          }
        }
    }
    
    else{
      uporabnik = res[0].substring(0, res[0].length - 1);
      vzdevek = aliImaLokalniVzdevek(uporabnik);
      if(!vzdevek){
        novElement = divElementEnostavniTekst(sporocilo.besedilo);
      }
      else{
        res[0] = vzdevek + ' (' + uporabnik + ')' + ":";
        novoSporocilo = res.join(" ");
        novElement = divElementEnostavniTekst(novoSporocilo);
      }
    }

    var slikeHtml = izlusciSlike(sporocilo.besedilo);
    
    $('#sporocila').append(novElement);
    if(slikeHtml){
       $('#sporocila').append(slikeHtml);
    }
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      var vzdevek = aliImaLokalniVzdevek(uporabniki[i]);
      if(!vzdevek){
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
      else{
        vzdevek = vzdevek + ' (' + uporabniki[i] + ')';
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(vzdevek));
      }
    }
    
    // Klik na ime uporabnika v seznamu zahteva posiljanje simbola
    $('#seznam-uporabnikov div').click(function(){
      var nadimek = $(this).text();
      var res = nadimek.split(" ");
      if(res.length > 1){
        var uporabnik = res[1];
        uporabnik = uporabnik.substring(1, uporabnik.length - 1);
      }
      else{
        uporabnik = nadimek;
      }
      var string = '/zasebno ' + '"' + uporabnik + '" ' + '"' + '&#9756;' + '"';
      //console.log(string);
      var sistemskoSporocilo = klepetApp.procesirajUkaz(string);
      if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
    });
  });
  
  function aliImaLokalniVzdevek(uporabnik){
    for(var j in tabelaUporabnikovVzdevki){
        if(uporabnik == tabelaUporabnikovVzdevki[j]){
          return tabelaVzdevkov[j];
        }
    }
    return false;
  }
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
