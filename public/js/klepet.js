// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */

var tabelaUporabnikovVzdevki = [];
var tabelaVzdevkov = [];

function aliZeImaVzdevek(uporabnik){
  for(var i in tabelaUporabnikovVzdevki){
    if(uporabnik == tabelaUporabnikovVzdevki[i]){
      return i;
    }
  }
  return false;
}

function aliImaLokalniVzdevek(uporabnik){
  for(var j in tabelaUporabnikovVzdevki){
      if(uporabnik == tabelaUporabnikovVzdevki[j]){
        return tabelaVzdevkov[j];
      }
  }
  return false;
}

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;
  var besedilo;
  var parametri;
  
  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      besedilo = besede.join(' ');
      parametri = besedilo.split('\"');
      if (parametri) {
        var lokalniVzdevek = aliImaLokalniVzdevek(parametri[1]);
        if(!lokalniVzdevek){
          sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3];
          this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
        }
        else{
          lokalniVzdevek = lokalniVzdevek + ' (' + parametri[1] + ')';
          sporocilo = '(zasebno za ' + lokalniVzdevek + '): ' + parametri[3];
          this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
        }
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    case 'preimenuj':
      besede.shift();
      besedilo = besede.join(' ');
      parametri = besedilo.split('\"');
      if(parametri[1] == trenutniVzdevek){
        sporocilo = "Nadimke lahko nastavljaš le drugim, sebi spremeni vzdevek.";
        break;
      }
      if (parametri) {
        var indeksUporabnika = aliZeImaVzdevek(parametri[1]);
        if(!indeksUporabnika){
          tabelaUporabnikovVzdevki.push(parametri[1]);
          tabelaVzdevkov.push(parametri[3]);
          sporocilo = "Uporabniku " + "'" + parametri[1] + "'" + " si nastavil nadimek " + "'" + parametri[3] + "'" + ".";
        }
        else{
          tabelaVzdevkov[indeksUporabnika] = parametri[3];
          sporocilo = "Uporabniku " + "'" +parametri[1] + "'" + " si spremenil nadimek v " + "'"+ parametri[3] + "'" + ".";
        }
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    case 'barva':
      besede.shift();
      var barva = besede.join(' ');
      document.querySelector('#kanal').style.backgroundColor = barva;
      document.querySelector('#sporocila').style.backgroundColor = barva;
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};